package com.epam.webexclient.start;

import com.epam.webexclient.logic.WebexClient;


public class WebexClientStart {
	
	private static final int SERVER_PORT = 8090;
	private static final String SERVER_ADDRESS = "localhost";
	private static final String HTTP_REQUEST = "GET /webex/controller?param1=hello%20world HTTP/1.1\n"
			+ "User-Agent: Java Super puper Application\n"
			+ "Host: "+SERVER_ADDRESS+"\n"
			+ "Accept-Language: en-us\n"
			+ "Accept-Encoding: gzip, deflate\n"
			+ "Connection: Close\n\n";
	
	public static void main(String[] args) {
		new WebexClient().proccessHttpRequest(SERVER_ADDRESS, SERVER_PORT, HTTP_REQUEST);
	}
	
}

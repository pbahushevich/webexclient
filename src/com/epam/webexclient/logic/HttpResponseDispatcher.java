package com.epam.webexclient.logic;

public class HttpResponseDispatcher {

	private static final String CONTENT_STRING = "\n\n-----------------------����������---------------------------------------\n";
	private static final String HEADER_STRING = "\n\n------------------------���������--------------------------------------------\n";
	private static final String STATUS_STRING = "\n\n------------------------������ ���������--------------------------------\n";

	public String parseServerResponse(String response) {

		StringBuilder result = new StringBuilder();
		String[] responseLines = response.split("\\n");

		result.append(getStatusLineRepresentation(responseLines));
		result.append(getHeadersRepresentation(responseLines));
		result.append(getContentRepresentation(responseLines));

		return result.toString();

	}
  
	private String getStatusLineRepresentation(String[] responseLines) {

		String statusLineSeparator = " ";

		int httpVersionPosition = 0;
		int httpCodePosition = 1;
		int reasonPosition = 2;

		String httpVersionString = "������ HTTP: ";
		String codeString = "\n��� ������ - ";
		String detailsString = "\n��������� - ";

		String statusLine = responseLines[httpVersionPosition];

		String[] statusDetails = statusLine.split(statusLineSeparator);
		String httpVersion = statusDetails[httpVersionPosition];
		String httpCode = statusDetails[httpCodePosition];
		String reasonPhrase = statusDetails[reasonPosition];

		StringBuilder result = new StringBuilder();

		result.append(STATUS_STRING).append(httpVersionString).append(httpVersion);
		result.append(codeString).append(httpCode).append(detailsString).append(reasonPhrase);

		return result.toString();
	}

	private String getHeadersRepresentation(String[] responseLines) {

		StringBuilder result = new StringBuilder();

		result.append(HEADER_STRING);
		for (int i = 1; i < responseLines.length; i++) {
			String currentHeader = responseLines[i];
			if ("".equals(currentHeader)) {
				break;
			}

			result.append(currentHeader);
			result.append(getHeaderComment(currentHeader));

		}

		return result.toString();
	}

	private String getHeaderComment(String curHeader) {

		String serverHeaderString = "Server";
		String serverHeaderComment = "- ���������� � �������\n";
		String cookieString = "Set-Cookie";
		String cookieComment = "- ����\n";
		String contentTypeString = "Content-Type";
		String contentTypeComment = "- ��� ����������� � �������� ���������\n";
		String contentLanguageString = "Content-Language";
		String contentLanguageComment = "- ���� �����������\n";
		String contentLengthString = "Content-Length";
		String contentLengthComment = "- ������ ����������� � ������\n";
		String connectionString = "Connection:";
		String connectionComment= "- ��������� ��� ��������� ����������\n";
		String dateString = "Date";
		String dateComment = "- ���� ������\n";
		String otherHeaderComment = " - �� ���. �� ���������� https://en.wikipedia.org/wiki/List_of_HTTP_header_fields\n";
		String indent = "    ............................................";		 
		String result = "";

		if (curHeader.contains(serverHeaderString)) {
			result =  serverHeaderComment;
		}
		else if (curHeader.contains(cookieString)) {
			result = cookieComment;
		}
		else if (curHeader.contains(connectionString)) {
			result = connectionComment;
		}
		else if (curHeader.contains(contentTypeString)) {
			result = contentTypeComment;
		}
		else if (curHeader.contains(contentLanguageString)) {
			result = contentLanguageComment;
		}
		else if (curHeader.contains(contentLengthString)) {
			result = contentLengthComment;
		}
		else if (curHeader.contains(dateString)) {
			result = dateComment;
		}else{
			result = otherHeaderComment;
		}
		return indent + result;
	}

	private String getContentRepresentation(String[] responseLines) {

		String nextLine = "\n";

		StringBuilder result = new StringBuilder();
		result.append(CONTENT_STRING);

		int contentStartIndex = getContentStartIndex(responseLines);
		for (int i = contentStartIndex; i < responseLines.length; i++) {
			result.append(responseLines[i]).append(nextLine);
		}
		return result.toString();
	}

	private int getContentStartIndex(String[] responseLines) {

		int headersStart = 1;
		int headersIndex = headersStart;
		int lastCheckedLine = responseLines.length-1;
		
		for (; headersIndex < lastCheckedLine; headersIndex++) {
			if ("".equals(responseLines[headersIndex])) {
				break;
			}
		}
		
		int contentStartIndex = headersIndex+1;
		
		return contentStartIndex;
	}
}

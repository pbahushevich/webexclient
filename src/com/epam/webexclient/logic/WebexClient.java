package com.epam.webexclient.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class WebexClient {
	
	private static final int WAIT_FOR_RESPONSE_TIME = 1000;
	private static final String CONNECTED_MESSAGE = "Connected to the server.......";
	private static final String SENT_MESSAGE = "Sent request......";
	private static final String GET_RESPONSE_MESSAGE = "Got response....... \n";
	private static final String ERROR_MESSAGE = "Something went wrong..... look through the stack trace for details...";

	public void proccessHttpRequest(String serverAdress,int servertPort,String httpRequest){
	
		StringBuilder response = new StringBuilder();

		try (Socket socket = new Socket(serverAdress,servertPort)){
			
			System.out.println(CONNECTED_MESSAGE);
			
			socket.getOutputStream().write(httpRequest.getBytes());
			
			System.out.println(SENT_MESSAGE);
			
			Thread.sleep(WAIT_FOR_RESPONSE_TIME);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream( ),"UTF-8"));
	
			while (reader.ready()){
	
				response.append(reader.readLine()).append("\n");
				
			}
		
			System.out.println(GET_RESPONSE_MESSAGE);
			System.out.println(new HttpResponseDispatcher().parseServerResponse(response.toString()));
			
		} catch (IOException |InterruptedException e) {
			System.out.println(ERROR_MESSAGE);
			e.printStackTrace();
		}
	
}
}
